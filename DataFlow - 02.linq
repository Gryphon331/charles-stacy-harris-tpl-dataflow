<Query Kind="Program">
  <NuGetReference>Microsoft.Tpl.Dataflow</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Threading.Tasks.Dataflow</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

async Task Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	int i = 0;

	var paymentProcessor = new TransformBlock<Payment, Receipt>(async payment =>
	{
		// Simulate some processing.
		await Task.Delay(50);
		return new Receipt();
	});
	
	var receiptProcessor = new ActionBlock<Receipt>(async receipt =>
	{
		// Simulate receipt processing.
		await Task.Delay(25);
		Interlocked.Increment(ref i);
	});
	
	// ***** 
	// This automatically links the output from one buffer into the other!
	// The disposable lets us remove links or destroy the entire network.
	IDisposable network = paymentProcessor.LinkTo(receiptProcessor, new DataflowLinkOptions{ PropagateCompletion = true });

	paymentProcessor.Dump();
	receiptProcessor.Dump();

	var sw = Stopwatch.StartNew();
	
	foreach (var payment in paymentList)
		paymentProcessor.Post(payment);
	
	paymentProcessor.Complete();

	//*** Now completion can occur.
	await receiptProcessor.Completion;
	network.Dispose();
	sw.Stop();
	sw.Dump("Done");
	i.Dump("Receipts processed");
	"Let's look at some more blocks.".Dump("Nice!");
}


public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
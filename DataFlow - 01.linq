<Query Kind="Program">
  <NuGetReference>Microsoft.Tpl.Dataflow</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Threading.Tasks.Dataflow</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

async Task Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	// *****
	// Enter the TPL!
	// Let's talk about the blocks, starting with the TransformBlock. Back to the whiteboard.

	int i = 0;
	// Note: This becomes the most general form of that match up with the buffer and task that we talked about.
	// Let's try some lambda style while we're at it.
	var paymentProcessor = new TransformBlock<Payment, Receipt>(async payment =>
	{
		Interlocked.Increment(ref i);
		// Simulate some processing.
		await Task.Delay(50);
		return new Receipt();
	});

	paymentProcessor.Dump();

	var sw = Stopwatch.StartNew();
	
	foreach (var payment in paymentList)
		paymentProcessor.Post(payment);
	
	paymentProcessor.Complete();

	// Note: This will never happen in this configuration. Don't worry, we'll fix it soon. :)
	await paymentProcessor.Completion;
	sw.Stop();
	sw.Dump("Done");
	i.Dump("Receipts processed");
	"Yes! Let's add some cowbell!".Dump("Can't we make this faster?");
}


public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
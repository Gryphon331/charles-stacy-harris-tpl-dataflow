<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
</Query>

void Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	var sw = Stopwatch.StartNew();
	
	foreach (var payment in paymentList)
	{
		var receipt = ProcessPayment(payment);
		// Do something with receipt.
	}
	
	sw.Stop();
	sw.Dump("Done");
	
	// How can we make this run faster with threads?
}

public Receipt ProcessPayment(Payment payment)
{
	// Simulate some processing that blocks a thread. Typically some sort of I/O operation.
	// Is blocking a thread okay? What are the pros and cons?
	Thread.Sleep(50);
	return new Receipt();
}

public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
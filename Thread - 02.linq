<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
</Query>

void Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	var sw = Stopwatch.StartNew();

	var doneEvent = new AutoResetEvent(false);
	int count = paymentList.Count;
	
	var receiptList = new List<Receipt>();
	
	foreach (var payment in paymentList)
	{
		// If you wanted to, you could create your own threads at this point and manage them yourself.
		ThreadPool.QueueUserWorkItem( _ =>
		{
			var receipt = ProcessPayment(payment);		
			// Do something with receipt.
			// Maybe add it to a list for later processing...
			receiptList.Add(receipt);
			
			if (Interlocked.Decrement(ref count) == 0)
				doneEvent.Set();
		});
	}
	
	doneEvent.WaitOne();	
	receiptList.Count.Dump("Oh no, something went wrong!");

	sw.Stop();
	sw.Dump("Done");
}

public Receipt ProcessPayment(Payment payment)
{
	// Simulate some processing that blocks a thread. Typically some sort of I/O operation.
	// Is blocking a thread okay? What are the pros and cons?
	Thread.Sleep(50);
	return new Receipt();
}

public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
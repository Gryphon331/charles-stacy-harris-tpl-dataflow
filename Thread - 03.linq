<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
</Query>

void Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	var sw = Stopwatch.StartNew();

	var doneEvent = new AutoResetEvent(false);
	int count = paymentList.Count;
	
	// Let's make this a blocking collection.
	var receiptList = new BlockingCollection<Receipt>();

	foreach (var payment in paymentList)
	{
		ThreadPool.QueueUserWorkItem( _ =>
		{
			var receipt = ProcessPayment(payment);
			// Do something with receipt.
			// Maybe add it to a list for later processing...
			receiptList.Add(receipt);
			
			//Note: We also could have added locking in the thread itself.
			
			if (Interlocked.Decrement(ref count) == 0)
				doneEvent.Set();
		});
	}
	
	doneEvent.WaitOne();
	receiptList.Count.Dump("Ahh, that's better!");

	sw.Stop();
	sw.Dump("Done");
	
	"What if we had thousands of payments or more to process?".Dump("What's wrong with this approach?");
}

public Receipt ProcessPayment(Payment payment)
{
	// Simulate some processing that blocks a thread. Typically some sort of I/O operation.
	// Is blocking a thread okay? What are the pros and cons?
	Thread.Sleep(50);
	return new Receipt();
}

public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
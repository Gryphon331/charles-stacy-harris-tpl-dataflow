<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

/*
	We can clean this up a bit:
	 - We no longer need the AutoResetEvent.
	 - We don't need to keep track of the count of messages processed, whew! Dodged that bullet!
*/


async Task Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	var sw = Stopwatch.StartNew();
	
	// Let's make this a blocking collection.
	var receiptList = new BlockingCollection<Receipt>();
	
	// We'll set a bounded capacity for the buffer
	var inputBuffer = new BlockingCollection<Payment>(20);
	
	var task = Task.Factory.StartNew(async () =>
	{
		while(!inputBuffer.IsCompleted)
		{
			Payment payment;
			if(inputBuffer.TryTake(out payment))
			{
				var receipt = await ProcessPayment(payment); //***** Enter the Task!
				// Do something with receipt.
				receiptList.Add(receipt);				
			}
		}
	});

	foreach (var payment in paymentList)
	{
		inputBuffer.Add(payment);
	}
	
	inputBuffer.CompleteAdding();	
	
	await task;
	Debug.Print("Stop");
	receiptList.Count.Dump("Receipts processed");

	sw.Stop();
	sw.Dump("Done");
	
	"We're not blocking threads anymore, but we need more cowbell! Er, um concurrency.".Dump("Where are we now?");
}

public async Task<Receipt> ProcessPayment(Payment payment)
{
	// Simulate some processing. Typically some sort of I/O operation.

	await Task.Delay(50); // ***** No more Thread blocking! Why is this?
	return new Receipt();
}

public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
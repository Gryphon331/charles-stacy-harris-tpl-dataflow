<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
</Query>

void Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	var sw = Stopwatch.StartNew();

	var doneEvent = new AutoResetEvent(false);
	int count = paymentList.Count;
	
	// Let's make this a blocking collection.
	var receiptList = new BlockingCollection<Receipt>();
	
	// **********
	// Let's prevent flooding the thread pool by putting a buffer in front of it.
	// We'll set a bounded capacity for the buffer
	var inputBuffer = new BlockingCollection<Payment>(20); // ***** 20 in the chamber
	
	for (int i = 0; i < 20; i++) // ***** 20 threads flying free
	{
		ThreadPool.QueueUserWorkItem( _ =>
		{
			// **********
			// Now the thread will have to loop
			
			Payment payment;
			while(!inputBuffer.IsCompleted)
			{
				if(inputBuffer.TryTake(out payment))
				{
					var receipt = ProcessPayment(payment);
					// Do something with receipt.
					// Maybe add it to a list for later processing...
					receiptList.Add(receipt);
					
					//Note: We also could have added locking in the thread itself.
					
					if (Interlocked.Decrement(ref count) == 0)
						doneEvent.Set();
				}
			}
		});

	}

	foreach (var payment in paymentList)
	{
		inputBuffer.Add(payment);
	}
	
	inputBuffer.CompleteAdding();
	doneEvent.WaitOne();
	receiptList.Count.Dump("Receipts processed");

	sw.Stop();
	sw.Dump("Done");
	
	"What do those threads do while the payment is processing?".Dump("What's wrong with this approach?");
}

public Receipt ProcessPayment(Payment payment)
{
	// Simulate some processing that blocks a thread. Typically some sort of I/O operation.
	// Is blocking a thread okay? What are the pros and cons?
	Thread.Sleep(50); // ***** Flying free until until they block!
	return new Receipt();
}

public class Payment
{
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}
<Query Kind="Program">
  <NuGetReference>Microsoft.Tpl.Dataflow</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <NuGetReference>Rx-Main</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Bson</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json.Schema</Namespace>
  <Namespace>Newtonsoft.Json.Serialization</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Threading.Tasks.Dataflow</Namespace>
  <Namespace>System.Reactive.Disposables</Namespace>
</Query>

/*
	Okay, this is going to be a bigger one.
	We'll also introduce the idea of Link filtering.
	First, "To the whiteboard Robin!"
*/

async Task Main()
{
	var jsonString = File.ReadAllText("C:/Users/stacyh/SkyDrive/Data Files/TestData.json");
	var paymentList = JsonConvert.DeserializeObject<List<Payment>>(jsonString);

	int i = 0;

	var paymentBuffer = new BufferBlock<Payment>();
	
	var transformOptions = new ExecutionDataflowBlockOptions{ MaxDegreeOfParallelism = 4 };

	var creditCardProcessor = new TransformBlock<Payment, Receipt>(async payment =>
	{
		// Simulate some processing.
		await Task.Delay(50);
		return new Receipt();
	}, transformOptions);
	
	var giftCardProcessor = new TransformBlock<Payment, Receipt>(async payment =>
	{
		// Simulate some processing.
		await Task.Delay(50);
		return new Receipt();
	}, transformOptions);

	var receiptProcessor = new ActionBlock<Receipt>(async receipt =>
	{
		// Simulate receipt processing.
		await Task.Delay(25);
		Interlocked.Increment(ref i);
	}, new ExecutionDataflowBlockOptions{MaxDegreeOfParallelism = 4});

	
	// *****
	// A composite disposable will help us dispose the whole network at once. This comes from System.Reactive.Disposables
	// If you want to be able to unhook specific links, you wouldn't want to put them all together like this.	
	var linkOptions = new DataflowLinkOptions{ PropagateCompletion = true }; 
	var network = new CompositeDisposable(
	
		paymentBuffer.LinkTo(creditCardProcessor, linkOptions, payment => payment.PaymentType == PaymentType.CreditCard),
	
		paymentBuffer.LinkTo(giftCardProcessor, linkOptions, payment => payment.PaymentType == PaymentType.GiftCard),
		
		// Note: We don't want to propagate completions here. Why is that?
		creditCardProcessor.LinkTo(receiptProcessor),
		giftCardProcessor.LinkTo(receiptProcessor)
		);

	paymentBuffer.Dump();
	creditCardProcessor.Dump("Credit card");
	giftCardProcessor.Dump("Gift card");
	receiptProcessor.Dump();

	var sw = Stopwatch.StartNew();
	
	foreach (var payment in paymentList)
		paymentBuffer.Post(payment);
	
	paymentBuffer.Complete();

	//*** Now completion can occur.
	await creditCardProcessor.Completion;
	await giftCardProcessor.Completion;
	receiptProcessor.Complete();
	await receiptProcessor.Completion;
	network.Dispose();
	sw.Stop();
	sw.Dump("Done");
	i.Dump("Receipts processed");
	"Where's the graphical designer for this, Stacy!?".Dump("I know what some of you are thinking...");
}

public enum PaymentType
{
	CreditCard,
	GiftCard
}

public class Payment
{
    public PaymentType PaymentType { get; set; }
	public decimal Amount { get; set; }
	public string CardNumber { get; set; }
	public string EmailAddress { get; set; }
}

public class Receipt
{
	public bool Approved { get; set; }
	public decimal AmountApproved { get; set; }
	public string EmailAddress { get; set; }
}